import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react-swc';

export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      '@': '/src', // this maps to your project root
      '@api': '/src/api',
      '@assets': '/src/assets',
      '@components': '/src/components',
      '@config': '/src/config',
      '@library': '/src/library',
      '@helpers': '/src/helpers',
      '@hooks': '/src/hooks',
      '@middlewares': '/src/middlewares',
      '@pages': '/src/pages',
      '@providers': '/src/providers',
      '@routes': '/src/routes',
      '@store': '/src/store',
      '@type': '/src/types',
      '@theme': '/src/theme',
      '@utils': '/src/utils',
    },
  },
});
