import { axiosClient } from '@config/axios';

export const getRefreshedAccessToken = async () => {
  const {
    data: { data: userData },
  } = await axiosClient.get('/users/refresh');

  return userData;
};

export const signout = async () => {
  await axiosClient.get('/users/signout');
};
