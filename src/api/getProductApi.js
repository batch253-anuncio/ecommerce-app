import axios from 'axios';

export const getProductApi = async ({ page = 1, limit = 10 }) => {
  const { data } = await axios.get(
    `http://localhost:1337/api/v1/products?page=${page}&limit=${limit}`,
  );

  return data;
};
