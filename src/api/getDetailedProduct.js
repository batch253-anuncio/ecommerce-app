import axios from 'axios';

export const getDetailedProducts = async (id) => {
  const {
    data: { data: product },
  } = await axios.get(`http://localhost:1337/api/v1/products/${id}`);

  return product;
};
