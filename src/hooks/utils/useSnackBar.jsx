import { toast } from 'react-toastify';

const useSnackBar = () => {
  const notify = (message) => toast(message);
  return notify;
};

export default useSnackBar;
