import { useQuery } from '@tanstack/react-query';

import { useSetUser } from '@store/useAuth';
import { getRefreshedAccessToken } from '@/api/authentication';

export const useRefreshAccessToken = () => {
  const setUser = useSetUser();

  const user = useQuery({
    queryKey: ['refresh-access-token'],
    queryFn: getRefreshedAccessToken,
    onSuccess: (userData) => {
      setUser(userData);
    },
    retry: false,
  });

  return user;
};
