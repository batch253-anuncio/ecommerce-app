import { appProductApi } from '@/api/appProductApi';

import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'react-toastify';

const useAddProductToCart = (refetch) => {
  const queryClient = useQueryClient();

  const status = useMutation({
    mutationFn: appProductApi,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['cart', 'product'] });
      refetch();
      toast.success('Product Added to Cart');
    },
    onError: (error) => {
      console.log(error);
    },
    retry: 0,
  });

  return status;
};

export default useAddProductToCart;
