import { useQuery } from '@tanstack/react-query';

import { getDetailedProducts } from '@/api/getDetailedProduct';

const useProductDetail = (productId) => {
  const productQuery = useQuery({
    queryKey: ['product'],
    queryFn: () => getDetailedProducts(productId),
  });

  const product = productQuery.isLoading ? [] : productQuery.data;

  return { ...productQuery, product };
};

export default useProductDetail;
