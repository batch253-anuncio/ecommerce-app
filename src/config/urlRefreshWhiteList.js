export const urlRefreshWhitelist = [
  '/users/refresh',
  '/users/signin',
  '/users/signout',
  '/users/signup',
];
