// library

import { CssBaseline } from '@mui/material';
import { ThemeProvider } from '@mui/material/styles';

// component
import appTheme from '@theme/index';

const MuiThemeProvider = ({ children }) => {
  return (
    <ThemeProvider theme={appTheme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

export default MuiThemeProvider;
