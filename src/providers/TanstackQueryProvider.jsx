// Library
import { QueryClientProvider } from '@tanstack/react-query';

// modules
import queryClient from '@config/TanstackConfig';

const TanstackQueryProvider = ({ children }) => {
  return (
    <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
  );
};

export default TanstackQueryProvider;
