import { Box } from '@mui/material';
import React from 'react';

const ProductPageWrapper = ({ children }) => {
  return (
    <Box width="100%">
      <Box
        maxWidth="1200px"
        display="flex"
        justifyContent="center"
        alignItems="flex-start  "
        marginX="auto"
        position="relative"
      >
        {children}
      </Box>
    </Box>
  );
};

export default ProductPageWrapper;
