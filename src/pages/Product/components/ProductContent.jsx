import React, { useState } from 'react';

import useGetProducts from '@hooks/products/useGetProduct';
import { Box } from '@mui/material';
import ProductCard from '@components/ProductCard';

import ProductSkeletonLoader from '@components/ProductCard/components/ProductSkeletonLoader';

import useGetCart from '@pages/Cart/hooks/useGetCart';
import { useUser } from '@store/useAuth';
import { generateSkeletonLoader } from '@utils/generateSkeletonLoader';

const ProductContent = () => {
  const [page] = useState(1);
  const [productSize] = useState(9);

  const { products, isLoading, isError } = useGetProducts(page, productSize);

  const user = useUser();
  const hasUser = !!user?.email && user.isAdmin === false;

  const {
    isLoading: isLoadingCart,
    isError: isCartError,
    refetch,
    data,
  } = useGetCart(hasUser);

  const isMappable = !isLoading && !isLoadingCart && !isError && !isCartError;

  return (
    <Box
      width="100%"
      display="flex"
      flexWrap="wrap"
      justifyContent="center"
      gap={3}
    >
      {hasUser
        ? isMappable
          ? products.map((product, i) => {
              const isInCart = data.itemIds.includes(product._id);
              return (
                <ProductCard
                  key={i}
                  item={product}
                  isInCart={isInCart}
                  refetch={refetch}
                />
              );
            })
          : generateSkeletonLoader(12, <ProductSkeletonLoader />)
        : isLoading
        ? generateSkeletonLoader(12, <ProductSkeletonLoader />)
        : products.map((product, i) => {
            return <ProductCard key={i} item={product} isInCart={false} />;
          })}
    </Box>
  );
};

export default ProductContent;
