import ProductDetailWrapper from './components/ProductDetailWrapper';

import { useParams } from 'react-router-dom';

import useProductDetail from '@hooks/products/useProductDetail';
import ProductDetailContentWrapper from './components/ProductDetailContentWrapper';
import ProductDetailContent from './components/ProductDetailContent';
import ProductDetailSkeletonLoader from './components/ProductDetailSkeleton';

const ProductDetail = () => {
  const { productId } = useParams();
  const { product, isLoading } = useProductDetail(productId);

  if (isLoading) {
    return <ProductDetailSkeletonLoader />;
  }
  return (
    <>
      <ProductDetailWrapper>
        <ProductDetailContentWrapper>
          <ProductDetailContent product={product} />
        </ProductDetailContentWrapper>
      </ProductDetailWrapper>
    </>
  );
};

export default ProductDetail;
