import React, { Fragment, useState } from 'react';
import { Box, Rating, Stack, Typography, IconButton } from '@mui/material';

import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';

import { LoadingButton } from '@mui/lab';
import useAddProductToCart from '@hooks/products/useAddProductToCart';
import { useUser } from '@store/useAuth';
import useCheckout from '@pages/Cart/hooks/useCheckout';
import { toast } from 'react-toastify';
import useGetCart from '@pages/Cart/hooks/useGetCart';

const ProductDetailContent = ({ product }) => {
  const user = useUser();
  const { refetch } = useGetCart(!!user?.email);
  const [quantity, setQuantity] = useState(1);
  const { mutate: addProductToCart, isLoading } = useAddProductToCart(refetch);

  const { mutate: processCheckout, isLoading: isLoadingCheckout } =
    useCheckout();

  const hasUser = user?.email && user?.isAdmin === false;

  const handleAddQuantity = () => {
    setQuantity((prev) => prev + 1);
  };
  const handleSubQuantatiy = () => {
    if (quantity <= 1) setQuantity(1);
    else setQuantity((prev) => prev - 1);
  };

  const handleAddToCart = () => {
    const {
      _id: productId,
      title,
      image,
      category,
      description,
      price,
    } = product;
    const cartData = [
      {
        productId,
        title,
        category,
        description,
        price,
        image,
        quantity,
      },
    ];

    addProductToCart(cartData);
  };

  const handleCheckout = () => {
    const cartItems = [
      {
        productId: product._id,
        title: product.title,
        image: product.image,
        price: product.price,
        quantity,
      },
    ];

    if (hasUser) {
      const checkOutData = {
        userId: user?._id,
        buyItNow: 'yes',
        cartItems,
      };

      processCheckout(checkOutData);
    } else {
      toast.error("No User or Admin can't make a purchase");
    }
  };

  return (
    <Fragment>
      <Box width="300px" flex={1}>
        <Box
          component="img"
          sx={{ width: '100%', objectFit: 'cover' }}
          src={product.image}
        />
      </Box>
      <Stack spacing={1} flex={2}>
        <Typography variant="h4" fontWeight={600} letterSpacing={3}>
          {product.title}
        </Typography>
        <Rating name="read-only" value={product.rating} readOnly />

        <Stack paddingTop={4} spacing={4}>
          <Typography variant="p">{product.description}</Typography>

          <Box display="flex" gap={1} alignItems="center">
            <IconButton
              onClick={handleSubQuantatiy}
              sx={{
                '&:hover': {
                  color: ({ palette }) => palette.primary.main,
                },
              }}
            >
              <RemoveIcon />
            </IconButton>

            {quantity}
            <IconButton
              onClick={handleAddQuantity}
              sx={{
                '&:hover': {
                  color: ({ palette }) => palette.primary.main,
                },
              }}
            >
              <AddIcon />
            </IconButton>
          </Box>

          <Box display="flex" gap={2}>
            <LoadingButton
              loading={isLoading}
              onClick={handleAddToCart}
              variant="outlined"
            >
              Add to Cart
            </LoadingButton>
            <LoadingButton
              loading={isLoadingCheckout}
              onClick={handleCheckout}
              variant="contained"
            >
              Buy it now
            </LoadingButton>
          </Box>
        </Stack>
      </Stack>
    </Fragment>
  );
};

export default ProductDetailContent;
