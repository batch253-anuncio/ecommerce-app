import { Box } from '@mui/material';

const ProductDetailContentWrapper = ({ children }) => {
  return (
    <Box display="flex" justifyContent="flex-start" gap={10}>
      {children}
    </Box>
  );
};

export default ProductDetailContentWrapper;
