import { Box } from '@mui/material';

const ProductDetailWrapper = ({ children }) => {
  return (
    <Box width="100%" marginY="50px">
      <Box width="1024px" marginX="auto">
        {children}
      </Box>
    </Box>
  );
};

export default ProductDetailWrapper;
