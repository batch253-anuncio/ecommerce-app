import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'react-toastify';
import { updateCartItemsquantity } from '../api/updateCartItemQuantity';

const useUpdateCartQuantity = (refetch) => {
  const queryClient = useQueryClient();

  const status = useMutation({
    mutationFn: updateCartItemsquantity,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['product'] });
      refetch();
      toast.success('Item Quantity Updated');
    },
    onError: (error) => {
      console.log(error);
    },
    retry: 0,
  });

  return status;
};

export default useUpdateCartQuantity;
