import { useMutation, useQueryClient } from '@tanstack/react-query';
import { deleteCart } from '../api/deleteCart';
import { toast } from 'react-toastify';

const useDeleteCart = () => {
  const queryClient = useQueryClient();

  const deleteQuery = useMutation({
    mutationFn: deleteCart,
    onSuccess: () => {
      queryClient.invalidateQueries(['cart']);
      toast.success('Item deleted successfully');
    },
  });

  return deleteQuery;
};

export default useDeleteCart;
