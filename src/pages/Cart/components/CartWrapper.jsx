import { Box } from '@mui/material';
import React from 'react';

const CartWrapper = ({ children }) => {
  return (
    <Box
      display="flex"
      justifyContent="center"
      maxWidth="1220px"
      marginX="auto"
    >
      {children}
    </Box>
  );
};

export default CartWrapper;
