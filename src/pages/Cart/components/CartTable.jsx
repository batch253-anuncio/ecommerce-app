import { useCallback, useEffect, useState } from 'react';

import useDebounce from '@hooks/utils/useDebounce';

import { Box, IconButton, Typography } from '@mui/material';
import { useUser } from '@store/useAuth';

// import useCartFunction from './hooks/useCartFunction';
import useGetCart from '../hooks/useGetCart';

import CartWrapper from './CartWrapper';

import HighlightOffIcon from '@mui/icons-material/HighlightOff';

import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';

import useUpdateCartQuantity from '../hooks/useUpdateCartQuantity';

import empty from '@assets/empty.webp';
import { LoadingButton } from '@mui/lab';
import useDeleteCart from '../hooks/useDeleteCart';
import useCheckout from '../hooks/useCheckout';

const CartTable = () => {
  const user = useUser();
  const { products, isLoading, total, refetch } = useGetCart(
    !!user?.email,
    1,
    10,
  );
  const { mutate: processCheckout } = useCheckout();

  if (isLoading) {
    return <p>Loading...</p>;
  }

  const isMappable = !isLoading && total;

  const handleCheckout = () => {
    const cartItems = products.cart[0].items.map((item) => {
      return {
        productId: item.productId,
        title: item.title,
        image: item.image,
        price: item.price,
        quantity: item.quantity,
      };
    });
    const checkOutData = {
      userId: products.userId,
      buyItNow: 'no',
      cartItems,
    };

    processCheckout(checkOutData);
  };

  return (
    <CartWrapper>
      {isMappable ? (
        <Box display="flex" flexDirection="column">
          <Table>
            <TableHead />
            <tbody>
              {products.cart[0]?.items.map((item, index) => (
                <Item item={item} key={index} refetch={refetch} />
              ))}
            </tbody>
            <TableFoot total={total} />
          </Table>

          <CheckoutButton handleCheckout={handleCheckout} />
        </Box>
      ) : (
        <CartEmpty />
      )}
    </CartWrapper>
  );
};

export default CartTable;

const Item = ({ item, refetch }) => {
  const [currentCount, setCurrentCount] = useState(item.quantity);
  const [countToUpdate, setCountToUpdate] = useState(null);

  const debouncedCount = useDebounce(currentCount);

  const { mutate } = useUpdateCartQuantity(refetch);
  const { mutate: deleteItem } = useDeleteCart();

  useEffect(() => {
    setCountToUpdate(debouncedCount);
  }, [debouncedCount, setCountToUpdate]);

  useEffect(() => {
    if (countToUpdate && item.quantity !== countToUpdate)
      mutate({ id: item.productId, quantity: countToUpdate });
  }, [countToUpdate, item.quantity]);

  const handleAdd = useCallback(() => {
    setCurrentCount((prev) => prev + 1);
  }, []);

  const handleSubtract = useCallback(() => {
    if (!currentCount >= 1) return;
    setCurrentCount((prev) => prev - 1);
  }, []);

  const handleDeleteItem = () => {
    deleteItem(item.productId);
  };

  return (
    <tr>
      <td>
        {/* DELETE */}
        <IconButton color="primary" onClick={handleDeleteItem}>
          <HighlightOffIcon />
        </IconButton>
      </td>
      <td style={{ paddingBlock: '30px' }}>
        <img src={item.image} style={{ width: '150px' }} />
      </td>

      <td align="left">{item.title}</td>
      <td>${item.price.toFixed(2)}</td>

      <td>
        {/* SUBTRACT */}
        <Box display="flex" alignItems="center" justifyContent="center" gap={2}>
          <IconButton
            onClick={handleSubtract}
            sx={{ paddingTop: '5px', marginX: '5px' }}
          >
            <RemoveIcon />
          </IconButton>

          {/* QUANTITY */}
          <Typography fontWeight={600}>{currentCount}</Typography>

          {/* ADD */}
          <IconButton
            onClick={handleAdd}
            sx={{ paddingTop: '5px', marginX: '5px' }}
          >
            <AddIcon />
          </IconButton>
        </Box>
      </td>

      <td align="right" style={{ paddingRight: '20px' }}>
        ${item.subTotal.toFixed(2)}
      </td>
    </tr>
  );
};

const TableHead = () => {
  return (
    <thead
      style={{
        backgroundColor: '#F8F8FF',
        borderRadius: '10px',
      }}
    >
      <tr style={{ paddingBlock: '5px' }}>
        <th></th>
        <th></th>
        <th align="left" style={{ paddingBlock: '5px' }}>
          Title
        </th>
        <th>Price</th>
        <th>Quantity</th>
        <th align="right" style={{ paddingRight: '20px' }}>
          Subtotal
        </th>
      </tr>
    </thead>
  );
};

const TableFoot = ({ total }) => {
  return (
    <tfoot>
      <tr>
        <td colSpan="100%" align="right" style={{ paddingRight: '20px' }}>
          <Typography fontWeight={600}> Total: ${total.toFixed(2)}</Typography>
        </td>
      </tr>
    </tfoot>
  );
};

const Table = ({ children }) => {
  return (
    <table
      style={{
        width: '840px',
        borderCollapse: 'collapse',
        textAlign: 'center',
        border: 'none',
        outline: 'none',
      }}
    >
      {children}
    </table>
  );
};

const CartEmpty = () => {
  return (
    <Box
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      height="50vh"
    >
      <img src={empty} style={{ width: '300px' }} alt="empty" />
      <Typography variant="p">No items in the cart</Typography>
    </Box>
  );
};

const CheckoutButton = ({ handleCheckout }) => {
  return (
    <LoadingButton
      onClick={handleCheckout}
      variant="contained"
      sx={{ marginLeft: 'auto', marginTop: '32px', width: '150px' }}
    >
      Check Out
    </LoadingButton>
  );
};
