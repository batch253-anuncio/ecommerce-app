import React from 'react';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import { IconButton } from '@mui/material';

const ItemQuantityTD = (itemQuantity) => {
  return (
    <td>
      <IconButton>
        <AddIcon />
      </IconButton>
      {itemQuantity}
      <IconButton>
        <RemoveIcon />
      </IconButton>
    </td>
  );
};

export default ItemQuantityTD;
