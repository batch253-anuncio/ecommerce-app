import { axiosClient } from '@config/axios';

export const updateCartItemsquantity = async ({ id, quantity }) => {
  const {
    data: { data: product },
  } = await axiosClient.patch(`/cart/${id}`, { quantity });

  return product;
};
