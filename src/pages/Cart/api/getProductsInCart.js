import { axiosClient } from '@config/axios';

export const getProductsInCart = async ({ page = 1, limit = 10 }) => {
  const {
    data: { data: cart },
  } = await axiosClient.get(`/cart/?page=${page}&limit=${limit}`);
  // console.log(cart);
  return cart;
};
