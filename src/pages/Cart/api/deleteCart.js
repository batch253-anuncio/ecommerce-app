import { axiosClient } from '@config/axios';

export const deleteCart = async (productId) => {
  const {
    data: { data: product },
  } = await axiosClient.delete(`/cart/${productId}`);

  return product;
};
