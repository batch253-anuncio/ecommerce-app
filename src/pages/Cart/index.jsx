import { Box, Typography } from '@mui/material';

import CartTable from './components/CartTable';
import CartImage from '@assets/Cart.jpg';

const Cart = () => {
  return (
    <Box>
      <Box
        sx={{
          background: `url(${CartImage})`,
          backgroundPosition: 'center',
          position: 'relative',
          marginBottom: '50px',
        }}
      >
        <Box
          sx={{
            backgroundColor: 'rgba(170, 0, 0, 0.6)',

            paddingY: '100px',
          }}
        >
          <Typography
            maxWidth="1220px"
            marginX="auto"
            variant="h2"
            fontWeight={700}
            color="white"
          >
            Shopping Cart
          </Typography>
        </Box>
      </Box>
      <CartTable />
    </Box>
  );
};

export default Cart;
