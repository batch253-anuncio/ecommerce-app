import useGetProducts from '@hooks/products/useGetProduct';
import { createContext, useCallback, useEffect, useState } from 'react';

export const ProductsContext = createContext({
  page: 1,
  pageSize: 1,
  changePageSize: () => {},
  changePage: () => {},
  productsQuery: {
    products: [],
    data: undefined,
    dataUpdatedAt: undefined,
    error: undefined,
    errorUpdatedAt: undefined,
    failureCount: undefined,
    failureReason: undefined,
    isError: false,
    isFetched: false,
    isFetchedAfterMount: false,
    isFetching: false,
    isPaused: false,
    isLoading: false,
    isLoadingError: false,
    isPlaceholderData: false,
    isPreviousData: false,
    isRefetchError: false,
    isRefetching: false,
    isInitialLoading: false,
    isStale: false,
    isSuccess: false,
    refetch: undefined,
    remove: undefined,
    status: undefined,
    fetchStatus: undefined,
  },
  count: 0,
  currentPage: 0,
});

const ProductsContextProvider = ({ children }) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const productsQuery = useGetProducts(page, pageSize);

  const count = productsQuery.data?.data.totalDocs || 0;
  const currentPage = productsQuery.data?.data.page
    ? productsQuery.data?.data.page - 1
    : 0;

  //   const hasNextPage = query.data?.next_page_url;
  //   usePrefetchClientGroups(hasNextPage, page, pageSize, searchString, filter);

  useEffect(() => {
    setPage(1);
  }, [pageSize]);

  const handleChangePage = useCallback((_, page) => {
    setPage(page + 1);
  }, []);

  const handleChangePageSize = useCallback((event) => {
    setPageSize(+event.target.value);
  }, []);

  return (
    <ProductsContext.Provider
      value={{
        page,
        pageSize,
        changePage: handleChangePage,
        changePageSize: handleChangePageSize,
        productsQuery,
        count,
        currentPage,
      }}
    >
      {children}
    </ProductsContext.Provider>
  );
};

export default ProductsContextProvider;
