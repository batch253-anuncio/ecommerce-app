import { useDropzone } from 'react-dropzone';

export default function Dropzone({ droppedImage, onDrop }) {
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    params: { addedfile: false },
  });

  return (
    <div
      {...getRootProps()}
      style={{
        border: '1px solid black',
        padding: '20px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <input {...getInputProps()} />
      {isDragActive ? (
        <p>Drop the files here ...</p>
      ) : (
        <p>Drag and drop a file here, or click to select a file</p>
      )}
      {droppedImage && (
        <img
          src={droppedImage}
          alt="Dropped Image"
          style={{ marginTop: '20px', maxWidth: '100%', height: '300px' }}
        />
      )}
    </div>
  );
}
