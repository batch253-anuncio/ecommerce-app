import z from 'zod';

export const addProductSchema = z.object({
  title: z.string().min(1, 'Title Required'),
  category: z.string().min(1, 'Category Required'),
  price: z.string().min(1),
  description: z.string().min(1, 'Description is required'),
  stock: z.string().optional(),
});
