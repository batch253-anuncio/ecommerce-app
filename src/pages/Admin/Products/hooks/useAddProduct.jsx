import { useMutation, useQueryClient } from '@tanstack/react-query';
import { addProductApi } from '../api/addProductApi';
import { addProductImageApi } from '../api/addProductImage';
import { toast } from 'react-toastify';

const useAddProduct = () => {
  const queryClient = useQueryClient();
  const addProductText = useMutation({
    mutationFn: addProductApi,
    onSuccess: (data) => {},
    onError: (error) => {
      console.log(error);
    },
    retry: 0,
  });

  const addProductImage = useMutation({
    mutationFn: (data) => {
      console.log(data);
      return addProductImageApi(data);
    },
    onSuccess: () => {
      queryClient.invalidateQueries(['product']);
      toast.success('Product successfully created');
    },
  });

  return { addProductText, addProductImage };
};

export default useAddProduct;
