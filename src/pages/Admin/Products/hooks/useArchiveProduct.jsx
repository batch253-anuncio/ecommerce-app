import { useMutation, useQueryClient } from '@tanstack/react-query';

import { toast } from 'react-toastify';
import { archiveProduct } from '../api/arhciveProductApi';

const useArchiveProduct = () => {
  const queryClient = useQueryClient();
  const ArichiveProduct = useMutation({
    mutationFn: archiveProduct,
    onSuccess: (data) => {
      const toastMessage = data.data?.isActive
        ? 'Product successfully archived'
        : 'Product successfully reinstated';

      toast.success(toastMessage);

      queryClient.invalidateQueries(['product']);
    },
  });
  return ArichiveProduct;
};

export default useArchiveProduct;
