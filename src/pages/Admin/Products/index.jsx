import { Box } from '@mui/material';
import ProductHeader from './components/ProductHeader';
import AdminProductTable from './components/ProductTable';
import ProductsContextProvider from './context/ProductsContext';

const ProductAdminPage = () => {
  return (
    <ProductsContextProvider>
      <Box
        width="100%"
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        gap="20px"
      >
        <ProductHeader />
        <AdminProductTable />
      </Box>
    </ProductsContextProvider>
  );
};

export default ProductAdminPage;
