import { axiosClient } from '@config/axios';

export const addProductImageApi = async (data) => {
  const formData = new FormData();
  formData.append('image', data.image);

  const {
    data: { data: product },
  } = await axiosClient.patch(`/products/uploads/${data._id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });

  return product;
};
