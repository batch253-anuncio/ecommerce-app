import { axiosClient } from '@config/axios';

export const addProductApi = async (data) => {
  data.price = +data.price;
  data.stock = +data.stock;
  const {
    data: { data: product },
  } = await axiosClient.post('/products/', data);

  return product;
};
