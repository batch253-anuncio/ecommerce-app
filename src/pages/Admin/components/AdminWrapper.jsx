import { Box } from '@mui/material';
import React from 'react';

const AdminWrapper = ({ children }) => {
  return <Box>{children}</Box>;
};

export default AdminWrapper;
