import { Link, Stack, Typography } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';

export const FormHeader = () => {
  return (
    <Stack width="100%" spacing={2}>
      <Typography fontSize={26} fontWeight={600}>
        Sign In
      </Typography>

      <Stack direction="row" spacing={0.5}>
        <Typography fontSize={16}>Don't have an account?</Typography>

        <Link
          component={RouterLink}
          to="/signup"
          underline="hover"
          fontSize={16}
          fontWeight={600}
        >
          Get started
        </Link>
      </Stack>
    </Stack>
  );
};
