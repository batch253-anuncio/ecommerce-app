import { axiosClient } from '@config/axios';

export const signInWithEmailAndPassword = async (data) => {
  const {
    data: { data: user },
  } = await axiosClient.post('/users/signin', data);

  return user;
};
