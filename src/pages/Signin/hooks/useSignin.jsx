import { useMutation } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';

import { useSetUser } from '@store/useAuth';
import { signInWithEmailAndPassword } from '../api/index';

const useSignin = () => {
  const navigate = useNavigate();
  const setUser = useSetUser();

  const signin = useMutation({
    mutationFn: signInWithEmailAndPassword,
    onSuccess: (userData) => {
      setUser(userData);
      navigate('/');
    },
  });

  return signin;
};

export default useSignin;
