// Library
import { useForm } from 'react-hook-form';
import { Box } from '@mui/material';
import { zodResolver } from '@hookform/resolvers/zod';

// custom hooks
import useSignin from './hooks/useSignin';

// components
import ControlledTextField from '@components/ControlledInputs/ControlledTextField';
import ContainerCentered from '@components/Misc/ContainerCentered';
import { FormHeader } from './components/FormHeader';
import ControlledPasswordTextField from '@components/ControlledInputs/ControlledPasswordTextField';
import FormContainer from '@components/Form/FormContainer';

import { signInFormSchema } from './schema/signInSchema';
import WrongEmailPasswordAlert from './components/WrongEmailPasswordAlert';
import BackToHome from './components/BackToHome';
import { SubmitButton } from '@components/Form/SubmitButton';
import RegisteredSuccessfully from './components/RegisteredSuccessFully';

const Signin = () => {
  const { mutate: submitForm, isLoading, error } = useSignin();
  const { control, handleSubmit } = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
    mode: 'onSubmit',
    resolver: zodResolver(signInFormSchema),
  });
  return (
    <ContainerCentered>
      <Box width="100%" maxWidth={550} margin="auto">
        <BackToHome />
        <FormContainer onSubmit={handleSubmit((data) => submitForm(data))}>
          <RegisteredSuccessfully />
          <FormHeader />
          <ControlledTextField
            size="small"
            control={control}
            name="email"
            label="Email"
            variant="filled"
            required
          />

          <ControlledPasswordTextField
            size="small"
            control={control}
            name="password"
            label="Password"
            variant="filled"
            required
          />

          <WrongEmailPasswordAlert error={error} />
          <SubmitButton isLoading={isLoading}>Sign In</SubmitButton>
        </FormContainer>
      </Box>
    </ContainerCentered>
  );
};

export default Signin;
