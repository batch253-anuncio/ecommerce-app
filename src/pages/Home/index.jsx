import Header from '@components/Layout/Header';
import { Box } from '@mui/material';

import HomeProductCard from './components/HomeProductCard';
import background from '@assets/Background.jpg';
import HomeBannerContent from './components/HomeBannerContent';
import HomeCategory from './components/HomeCategory';
import ShippingFeatures from './components/ShippingFeatures';

const Home = () => {
  return (
    <Box>
      <Box
        sx={{
          background: {
            xs: 'none',
            lg: `url(${background})`,
          },
          backgroundPosition: {
            lg: 'top',
          },
        }}
      >
        <Header />
        <HomeBannerContent />
      </Box>
      <HomeCategory />
      <HomeProductCard />
      <ShippingFeatures />
    </Box>
  );
};

export default Home;
