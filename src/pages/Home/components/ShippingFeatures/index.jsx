import { Box, Typography } from '@mui/material';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';

const ShippingFeatures = () => {
  return (
    <Box
      sx={{
        color: ({ palette }) => palette.primary.main,
        maxWidth: '1220px',
        marginX: 'auto',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Box
        sx={{
          '&:hover': {
            backgroundColor: '#a00',
            color: '#fff',
          },

          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          width: '300px',
          height: '150px',
        }}
      >
        <AccountBalanceWalletIcon />
        <Typography variant="p">Money Back Guarantee</Typography>
      </Box>
      <Box
        width="300px"
        height="150px"
        sx={{
          '&:hover': {
            backgroundColor: '#a00',
            color: '#fff',
          },

          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {' '}
        <AccountBalanceWalletIcon />
        <Typography variant="p">Money Back Guarantee</Typography>
      </Box>
      <Box
        width="300px"
        height="150px"
        sx={{
          '&:hover': {
            backgroundColor: '#a00',
            color: '#fff',
          },

          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <AccountBalanceWalletIcon />
        <Typography variant="p">Money Back Guarantee</Typography>
      </Box>

      <Box
        width="300px"
        height="150px"
        sx={{
          '&:hover': {
            backgroundColor: '#a00',
            color: '#fff',
          },

          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <AccountBalanceWalletIcon />
        <Typography variant="p">Money Back Guarantee</Typography>
      </Box>
    </Box>
  );
};

export default ShippingFeatures;
