import { Box, Stack } from '@mui/material';

import Home1 from '@assets/Home1.png';
import Home2 from '@assets/Home2.png';
import banner from '@assets/banner.jpg';

const HomeCategory = () => {
  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: '1024px',
        marginX: 'auto',
        paddingY: '150px',
        gap: '10px',
      }}
    >
      <Box
        sx={{
          background: `url(${Home1})`,
          flex: 1,
          height: '500px',
          maxWidth: '500px',
        }}
      ></Box>

      <Stack flex={1} spacing={1}>
        <Box
          sx={{
            background: `url(${Home2})`,
            backgroundPosition: '300px 100%',
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundColor: '#A00',
            height: '300px',
            paddingX: '100px',
          }}
        ></Box>

        <Box sx={{ width: '535px' }}>
          <Box>
            <img
              style={{ objectFit: 'cover', width: '100%' }}
              src={`${banner}`}
            />
          </Box>
        </Box>
      </Stack>
    </Box>
  );
};

export default HomeCategory;
