import axios from 'axios';

const RegisterUser = async (data) => {
  const {
    data: { data: user },
  } = await axios.post('http://localhost:1337/api/v1/users/signup', data);

  return user;
};

export default RegisterUser;
