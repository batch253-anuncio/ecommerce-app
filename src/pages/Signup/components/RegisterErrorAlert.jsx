import { useEffect, useState } from 'react';

import { Alert } from '@mui/material';

const RegisterErrorAlert = ({ error }) => {
  const [alert, setAlert] = useState(false);
  let alertTimer;

  const handleResetAlert = () => {
    setAlert(false);
    clearTimeout(alertTimer);
  };

  const handleSetAlert = () => {
    setAlert(true);

    alertTimer = setTimeout(() => {
      setAlert(false);
    }, 6_000);
  };

  useEffect(() => {
    if (error) {
      handleSetAlert();
    }
  }, [error]);

  useEffect(() => {
    return () => {
      clearTimeout(alertTimer);
    };
  }, []);

  if (alert)
    return (
      <Alert
        severity="error"
        onClose={handleResetAlert}
        sx={{
          width: '100%',
        }}
      >
        {error.response.status === 409
          ? 'Duplicate email found'
          : error.message}
      </Alert>
    );
};

export default RegisterErrorAlert;
