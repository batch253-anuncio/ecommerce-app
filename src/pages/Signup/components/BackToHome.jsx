import { Link as RouterLink } from 'react-router-dom';

import { Box, Link, Typography } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

const BackToHome = () => {
  return (
    <Box display="flex">
      <Link component={RouterLink} to="/" color="#808080">
        <ArrowBackIcon fontSize="small" />
        <Typography
          variant="p"
          letterSpacing="0.5px"
          fontSize="1.1rem"
          paddingBottom="6px"
        >
          Back To Home
        </Typography>
      </Link>
    </Box>
  );
};

export default BackToHome;
