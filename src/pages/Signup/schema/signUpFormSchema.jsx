import z from 'zod';

const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/;

export const signUpFormSchema = z.object({
  firstName: z.string().min(1, 'Please enter your First Name'),
  lastName: z.string().min(1, 'Please enter your Last Name'),
  email: z
    .string()
    .min(1, 'Please enter your email.')
    .email('Please enter a valid email'),
  password: z
    .string()
    .min(12, 'Password must have atleast 12 characters.')
    .regex(
      passwordRegex,
      'Password must have 1 capital and small letters, 1 number & 1 special character.',
    ),
});

export default signUpFormSchema;
