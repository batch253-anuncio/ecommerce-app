import { useMutation } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import RegisterUser from '../api';

const useSignup = () => {
  const navigate = useNavigate();

  const user = useMutation({
    mutationFn: RegisterUser,
    onSuccess: () => {
      navigate('/signin?register=success');
    },
  });
  return user;
};

export default useSignup;
