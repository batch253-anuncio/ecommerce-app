import { Navigate, Outlet, useLocation, useNavigate } from 'react-router-dom';

import Layout from '@components/Layout';
import { useUser } from '@store/useAuth';
import { useEffect } from 'react';

const PrivateRoute = () => {
  const user = useUser();
  const location = useLocation();
  const navigate = useNavigate();

  const isAuthenticated = !!user;

  if (!isAuthenticated) return <Navigate to="/signin" replace />;

  useEffect(() => {
    if (location.pathname === '/cart' && user?.isAdmin === true)
      return navigate('/');
  }, [location.pathname]);

  return <Layout isPrivateRoute={true}>{<Outlet />}</Layout>;
};

export default PrivateRoute;
