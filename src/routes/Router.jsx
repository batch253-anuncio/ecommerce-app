import { Routes, Route } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';

import routes from './routes';

import AdminRoute from './AdminRoute';
import PublicRoute from './PublicRoute';

const Router = () => {
  return (
    <Routes>
      {routes.map((route) => {
        const { path, Component, isPrivateRoute, adminRoute } = route;

        if (isPrivateRoute)
          return (
            <Route key={path} element={<PrivateRoute />}>
              <Route path={path} element={<Component />} />
            </Route>
          );
        if (adminRoute) {
          return (
            <Route key={path} element={<AdminRoute />}>
              <Route path={path} element={<Component />} />
            </Route>
          );
        }

        return (
          <Route key={path} element={<PublicRoute />}>
            <Route path={path} element={<Component />} />
          </Route>
        );
      })}
    </Routes>
  );
};

export default Router;
