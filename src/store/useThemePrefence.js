import { create } from 'zustand';
import { persist } from 'zustand/middleware';

const useThemePreference = create(
  persist(
    (set) => ({
      currentTheme: 'dark',
      isDark: true,
      toggleTheme: () =>
        set((state) => ({
          currentTheme: state.currentTheme === 'light' ? 'dark' : 'light',
          isDark: state.currentTheme === 'light',
        })),
    }),
    {
      name: 'theme-preference',
      // Make sure to include the `version` option to avoid version incompatibility issues
      version: 1,
      // Use localStorage as the storage provider
      getStorage: () => localStorage,
    },
  ),
);

export const useCurrentTheme = () =>
  useThemePreference((state) => state.currentTheme);

export const useToggleTheme = () =>
  useThemePreference((state) => state.toggleTheme);

export const useIsDark = () => useThemePreference((state) => state.isDark);
