export const generateSkeletonLoader = (count, SkeletonComponent) => {
  return Array(count)
    .fill(SkeletonComponent)
    .map((Component, index) => ({ ...Component, key: index }));
};
