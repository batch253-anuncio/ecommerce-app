import { LoadingButton } from '@mui/lab';

export const SubmitButton = ({ isLoading, children }) => {
  // TODO: Add a status notification if account is successfully created.
  return (
    <LoadingButton
      size="large"
      type="submit"
      variant="contained"
      fullWidth
      loading={isLoading}
      sx={{
        fontWeight: 600,
      }}
    >
      {children}
    </LoadingButton>
  );
};
