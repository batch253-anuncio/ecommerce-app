import { useEffect, useState } from 'react';

import { InputAdornment, TextField } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';

import useDebounce from '@hooks/utils/useDebounce';
import ClearSearchbarButton from './ClearSearchBarButton';

const Searchbar = ({ onSetSearch, ...props }) => {
  const [searchValue, setSearchValue] = useState('');

  const debouncedValue = useDebounce(searchValue);

  useEffect(() => {
    onSetSearch(debouncedValue);
  }, [debouncedValue]);

  const handleSetSearchValue = (event) => {
    setSearchValue(event.target.value);
  };

  const handleClearSearchValue = () => {
    setSearchValue('');
  };

  return (
    <TextField
      value={searchValue}
      onChange={handleSetSearchValue}
      {...props}
      label="Search"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        ),
        endAdornment: (
          <InputAdornment position="end">
            <ClearSearchbarButton
              show={!!searchValue}
              onClear={handleClearSearchValue}
            />
          </InputAdornment>
        ),
        ...props.InputProps,
      }}
    />
  );
};

export default Searchbar;
