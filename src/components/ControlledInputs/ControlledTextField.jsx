import { Controller } from 'react-hook-form';
import { TextField } from '@mui/material';

const ControlledTextField = (props) => {
  const { control, name, helperText, ...rest } = props;
  return (
    <Controller
      name={name}
      control={control}
      render={({ field, fieldState: { error } }) => (
        <TextField
          fullWidth
          error={!!error}
          {...rest}
          value={field.value}
          onChange={(e) => field.onChange(e.target.value)}
          helperText={error ? error.message : helperText}
        />
      )}
    />
  );
};

export default ControlledTextField;
