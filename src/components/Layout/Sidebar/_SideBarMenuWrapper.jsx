import { Box } from '@mui/material';
import React from 'react';

const SideBarMenuWrapper = ({ children }) => {
  return (
    <Box
      sx={{
        width: '100%',
        '&:hover': {
          backgroundColor: 'rgba(0,0,0,0.3)',
        },
        paddingX: '30px',
      }}
    >
      {children}
    </Box>
  );
};

export default SideBarMenuWrapper;
