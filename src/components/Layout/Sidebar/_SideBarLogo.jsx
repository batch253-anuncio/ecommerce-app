import { Box } from '@mui/material';

import logo2 from '@assets/logo2.png';
import SideBarSigninSignout from './_SideBarSignout';
import useSignout from '@hooks/authentication/useSignout';

const SideBarLogo = () => {
  const { mutate: signout, isLoading } = useSignout();

  const logoutHandler = () => {
    signout();
  };

  return (
    <Box marginLeft="30px" display="flex" alignItems="center" gap="10px">
      <Box width={100}>
        <Box
          component="img"
          src={logo2}
          sx={{
            width: '100%',
            objectFit: 'cover',
          }}
        />
      </Box>
      <SideBarSigninSignout isLoading={isLoading} logoutHandler={logoutHandler}>
        Sign out
      </SideBarSigninSignout>
    </Box>
  );
};

export default SideBarLogo;
