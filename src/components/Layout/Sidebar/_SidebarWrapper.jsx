import { Box } from '@mui/material';

const SidebarWrapper = ({ children }) => {
  return (
    <Box
      height="100vh"
      sx={{
        display: {
          xs: 'none',
          lg: 'block',
        },
        width: {
          lg: '250px',
        },
        backgroundColor: '#1C2536',
        color: 'white',
      }}
    >
      {children}
    </Box>
  );
};

export default SidebarWrapper;
