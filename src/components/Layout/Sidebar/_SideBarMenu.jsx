import { Fragment } from 'react';

import SideBarMenuWrapper from './_SideBarMenuWrapper';
import { menulist } from './MenuList';
import { Link as RouterLink } from 'react-router-dom';
import { Typography, Link, Box } from '@mui/material';

const SideBarMenu = () => {
  return (
    <Fragment>
      {menulist.map((menu, i) => (
        <SideBarMenuWrapper key={i}>
          <Link
            component={RouterLink}
            to={`/admin/${menu.name.toLowerCase()}`}
            sx={{
              textDecoration: 'none',
              display: 'flex',
              justifyContent: 'flex-start',
              alignItems: 'center',
              color: ({ palette }) => palette.common.white,
              paddingY: '5px',
              gap: 3,
            }}
          >
            <Box paddingTop="5px">{<menu.image />}</Box>
            <Typography variant="h6">{menu.name}</Typography>
          </Link>
        </SideBarMenuWrapper>
      ))}
    </Fragment>
  );
};

export default SideBarMenu;
