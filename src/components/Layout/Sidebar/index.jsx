import SideBarLogo from './_SideBarLogo';
import SideBarMenu from './_SideBarMenu';
import SidebarWrapper from './_SidebarWrapper';

const Sidebar = () => {
  return (
    <SidebarWrapper>
      <SideBarLogo />
      <SideBarMenu />
    </SidebarWrapper>
  );
};

export default Sidebar;
