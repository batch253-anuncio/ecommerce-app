import * as React from 'react';

import { Link as RouterLink } from 'react-router-dom';

import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import List from '@mui/material/List';

import { IconButton, Box, Link } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { MenuList } from '../HeaderMenu/_MenuList';

export default function BurgerMenu() {
  const [state, setState] = React.useState({
    top: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {MenuList.map((text, i) => (
          <Box
            key={i}
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            <Link
              sx={{
                textDecoration: 'none',
                color: ({ palette }) => palette.common.black,
                '&:hover': {
                  color: ({ palette }) => palette.common.black,
                },
                paddingY: '10px',
              }}
              component={RouterLink}
              to={`/${text.toLowerCase()}`}
            >
              {text}
            </Link>
          </Box>
        ))}
      </List>
    </Box>
  );

  return (
    <div>
      <React.Fragment key={'top'}>
        <IconButton
          onClick={toggleDrawer('top', true)}
          sx={{
            display: {
              md: 'none',
            },
          }}
        >
          <MenuIcon />
        </IconButton>
        <SwipeableDrawer
          anchor={'top'}
          open={state.top}
          onClose={toggleDrawer('top', false)}
          onOpen={toggleDrawer('top', true)}
        >
          {list('top')}
        </SwipeableDrawer>
      </React.Fragment>
    </div>
  );
}
