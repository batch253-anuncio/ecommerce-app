import { Box } from '@mui/material';
import Logo from '@assets/Logo.png';

const HeaderLogo = () => {
  return (
    <Box
      component="figure"
      sx={{
        width: 'clamp(90px, 5vw, 200px)',
      }}
    >
      <Box
        component="img"
        src={Logo}
        alt="logo"
        sx={{
          objectFit: 'cover',
          width: '100%',
        }}
      />
    </Box>
  );
};

export default HeaderLogo;
