import { Box } from '@mui/material';
import { useUser } from '@store/useAuth';

import BurgerMenu from '../HeaderBurgerNav/BurgerMenu';

import HeaderCart from './_HeaderCart';
import HeaderProfile from './_HeaderProfile';

const HeaderCartAndProfile = () => {
  const user = useUser();
  return (
    <Box display="flex" alignItems="center" gap={1}>
      {!user?.isAdmin && <HeaderCart />}
      <HeaderProfile />
      <BurgerMenu />
    </Box>
  );
};

export default HeaderCartAndProfile;
