import { Box } from '@mui/material';

const CartCounter = () => {
  return (
    <Box
      position="absolute"
      width="15px"
      top="0"
      right="0"
      borderRadius="100%"
      textAlign="center"
      sx={{
        backgroundColor: ({ palette }) => palette.primary.main,
        color: 'white',
        fontSize: '9px',
      }}
    >
      1
    </Box>
  );
};

export default CartCounter;
