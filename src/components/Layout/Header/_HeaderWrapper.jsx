import { Box } from '@mui/material';

const HeaderWrapper = ({ children }) => {
  return (
    <Box component="header" paddingY={1}>
      <Box
        maxWidth="1440px"
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginX="auto"
      >
        {children}
      </Box>
    </Box>
  );
};

export default HeaderWrapper;
