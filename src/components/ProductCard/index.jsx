import useAddProductToCart from '@hooks/products/useAddProductToCart';
import { LoadingButton } from '@mui/lab';
import { Link as RouterLink } from 'react-router-dom';
import { Box, Rating, Stack, Typography, Link } from '@mui/material';

const ProductCard = ({ item, isInCart, refetch }) => {
  const { mutate: addToCart, isLoading } = useAddProductToCart(refetch);

  const addProductToCart = () => {
    const { _id: productId, title, image, category, description, price } = item;

    const cartData = [
      {
        productId,
        title,
        category,
        description,
        price,
        image,
        quantity: 1,
      },
    ];
    addToCart(cartData);
  };

  return (
    <Stack
      display="flex"
      flexDirection="column"
      maxWidth={250}
      justifyContent="center"
      alignItems="center"
    >
      <Link component={RouterLink} to={`/product/detail/${item._id}`}>
        <Box width="250px" height="inherit">
          <img
            src={item.image}
            alt={item.title}
            style={{
              objectFit: 'cover',
              width: '100%',
            }}
          />
        </Box>
      </Link>
      <Stack
        spacing={0.5}
        display="flex"
        justifyContent="center"
        alignItems="center"
      >
        <Typography variant="h6">{item.title}</Typography>
        <Rating name="read-only" value={item.rating} readOnly />
        <Typography variant="body1">${item.price.toFixed(2)}</Typography>
        <LoadingButton
          onClick={addProductToCart}
          size="large"
          type="submit"
          variant="text"
          disabled={isInCart}
          loading={isLoading}
          sx={{
            fontWeight: 500,
            height: '20px',
            width: '140px',
          }}
        >
          {isInCart ? 'Added To Cart' : 'Add to Cart'}
        </LoadingButton>
      </Stack>
    </Stack>
  );
};

export default ProductCard;
