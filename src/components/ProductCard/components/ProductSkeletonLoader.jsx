import { Box, Skeleton } from '@mui/material';
import React from 'react';

const ProductSkeletonLoader = () => {
  return (
    <Box maxWidth={250}>
      <Skeleton variant="rectangular" width={250} height={250} />
      <Skeleton variant="text" />
      <Skeleton variant="text" />
      <Skeleton variant="text" />
      <Skeleton variant="text" />
    </Box>
  );
};

export default ProductSkeletonLoader;
