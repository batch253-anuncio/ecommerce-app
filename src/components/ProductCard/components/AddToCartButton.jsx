import { LoadingButton } from '@mui/lab';
import React from 'react';

const AddToCartButton = ({ children, isLoading, addProductToCart }) => {
  return (
    <LoadingButton
      size="large"
      type="submit"
      variant="text"
      onClick={addProductToCart}
      loading={isLoading}
      sx={{
        fontWeight: 500,
        height: '20px',
        width: '140px',
      }}
    >
      {children}
    </LoadingButton>
  );
};

export default AddToCartButton;
