import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';

const LoadingAnimation = styled(Box)(({ theme }) => ({
  '@keyframes rotate': {
    '0%': {
      transform: 'translate(-50%, -100%) rotate(0deg) scale(1, 1)',
    },
    '25%': {
      transform: 'translate(-50%, 0%) rotate(180deg) scale(1, 1)',
    },
    '45%, 55%': {
      transform: 'translate(-50%, 100%) rotate(180deg) scale(3, 0.5)',
    },
    '60%': {
      transform: 'translate(-50%, 100%) rotate(180deg) scale(1, 1)',
    },
    '75%': {
      transform: 'translate(-50%, 0%) rotate(270deg) scale(1, 1)',
    },
    '100%': {
      transform: 'translate(-50%, -100%) rotate(360deg) scale(1, 1)',
    },
  },

  position: 'relative',
  width: 164,
  height: 164,
  '&:before, &:after': {
    content: '""',
    position: 'absolute',
    width: 40,
    height: 40,
    backgroundColor: '#df9f1f',
    left: '50%',
    top: '50%',
    animation: 'rotate 1s ease-in infinite',
  },
  '&:after': {
    width: '20px',
    height: '20px',
    backgroundColor: '#FF3D00',
    animation: 'rotate 1s ease-in infinite, moveY 1s ease-in infinite',
  },
  '@keyframes moveY': {
    '0%, 100%': { top: '10%' },
    '45%, 55%': { top: '59%' },
    '60%': { top: '40%' },
  },
}));

export default LoadingAnimation;
